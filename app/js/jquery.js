// Menu Highlight
    
function menuHighlight(){
    // Store original location in loc like: http://test.com/one/ (ending slash)
    var loc = location.href; 
    // If the last char is a slash trim it, otherwise return the original loc
    loc = loc.lastIndexOf('/') == (loc.length -1) ? loc.substr(0,loc.length-1) : loc.substr(0,loc.lastIndexOf('/'));
    var targetValue = loc.substr(loc.lastIndexOf('/') + 1);
    //console.log(targetValue);
}


// Content Carousel
function carousel() {
    $('.productCarousel').slick({
           dots: false,
           infinite: true,
           variableWidth: true,
           arrows: true,
           infinite: true,
           draggable: false,
           slidesToShow: 3,
           slidesToScroll: 3
    });
    var carouselWidth = $(".slick-list").width() / 3;
    $(".productCarousel .product").width(carouselWidth);
    $(".hero-slider").slick({
           dots: true,
           infinite: true,
           variableWidth: true,
           arrows: false,
           infinite: false,
           draggable: false,
           autoplay: true,
           autoplaySpeed: 5000
    });
     
} 
function slideWidth() {
    var windowWidth = $(window).width();
    if (windowWidth < 1025) {
        $(".heroCarousel .homeHero").width(windowWidth);
    } else {
        $(".heroCarousel .homeHero").width("");
    }
}

function hamburgerAnimation() {
    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.header .navContainer .nav').slideToggle('open');
    });
}

function tooltips() {
    $('.header .navContainer .nav ul li a').each(function(){
        if ($(this).attr('title')) {
            $(this).append('<div class="toolTip"></div>');
            $(this).children('.toolTip').html($(this).attr('title'));
        }
    });
    $('.header .navContainer .nav ul li a').hover(function(){
        $(this).attr('title', '');
        
    });
}

function checkStrength(password) {
    var strength = 0
    if (password.length < 6) {
        $('#result').removeClass('')
        $('#result').addClass('short')
        return 'Too short'
    }
    if (password.length > 7) strength += 1
    // If password contains both lower and uppercase characters, increase strength value.
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
    // If it has numbers and characters, increase strength value.
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
    // If it has one special character, increase strength value.
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
    // If it has two special characters, increase strength value.
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
    // Calculated strength value, we can return messages
    // If value is less than 2
    if (strength < 2) {
        $('#result').removeClass()
        $('#result').addClass('weak')
        return 'Password Weak'
    } else if (strength == 2) {
        $('#result').removeClass()
        $('#result').addClass('good')
        return 'Password Good'
    } else {
        $('#result').removeClass()
        $('#result').addClass('strong')
        return 'Password Strong'
    }
}


function showMapOnClick(){
    var mapHeight = $(".locations").outerHeight() - 160;
    //console.log(mapHeight);
    $(".locations .overlay .overlay-contents iframe").height(mapHeight);
    
    var mapLocations = [
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2240.8354891946806!2d-3.958836833627596!3d55.83081468057416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48886c61a6ea2a57%3A0x6e663571587e0569!2sMotherwell%20ML1%205SB!5e0!3m2!1sen!2suk!4v1586342371346!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2608.594633803654!2d16.67457121619622!3d49.17030207931929!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4712eada02fd90b3%3A0xd32dbb9be9945572!2sTu%C5%99anka%201236%2F96%2C%20627%2000%20Slatina%2C%20Czechia!5e0!3m2!1sen!2suk!4v1586348066410!5m2!1sen!2suk",
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2748.246914392991!2d16.996277116129445!3d46.46361697912554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4768936ca9d5d3c9%3A0xff132821707f37bb!2sNagykanizsa%2C%20D%C3%B3zsa%20Gy%C3%B6rgy%20u.%2C%208800%20Hungary!5e0!3m2!1sen!2suk!4v1586348129603!5m2!1sen!2suk"
    ];
    
    $(".locations .location .content .icon").click(function(){
        var dataMapID = $(this).data("mapid");
        $(".overlay iframe").attr("src", mapLocations[dataMapID])
        $(".overlay").fadeIn();
        $(".overlay-close").click(function(){
            $(".overlay").fadeOut();
            $(".overlay iframe").attr("src", "");
        });
    });
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function productShowHide() {
    $(".productContent_tabs-internal a").click(function(e){
        e.preventDefault();
        var showID = $(this).attr("href");
        $(".productContent_tabs a").removeClass("active");
        $(this).addClass("active");
        $(".productContentSection").removeClass("active");
        $(showID).addClass("active");
    });
    
    // Show content dependant on the url
    var contentID = getUrlVars()["id"];
    
    if (contentID){
        var idShow = '#' + contentID;
        console.log(idShow)
        $(".productContentSection").removeClass("active");
        $(idShow).addClass("active"); 
        $(".productContent_tabs a").removeClass("active");
        $(".productContent_tabs ." + contentID).addClass("active");
    } else {
        
    }
    

    
}


function loadMore() {
    $('.support .columnPadding').hide();
    $('.support .columnPadding').slice(0, 6).show();
    if ($('.support .columnPadding').length <= 6 ) {
        $('.buttonLoadmore').hide();
    } else {
       $('.buttonLoadmore').show(); 
    }
    $('.buttonLoadmore').on('click', function (e) {
        e.preventDefault();
        $('.support .columnPadding:hidden').slice(0, 3).show();
        if ($('.support .columnPadding:hidden').length == 0) {
            $('.buttonLoadmore').hide();
        }
    });
}




$(document).ready(function(){
    hamburgerAnimation()
    //loadMore()
    carousel()
    $('#password').keyup(function() {
        $('#result').html(checkStrength($('#password').val()))
    });
    showMapOnClick()
    productShowHide()
});

$(window).resize(function(){
    //slideWidth()
})
    
   


